﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteVentas : Form
    {
        private DataSet dsVentas;
        private DataRow Ventas;
        public FrmReporteVentas()
        {
            InitializeComponent();
        }
        public DataSet Dsventas { get { return dsVentas; } set { dsVentas = value; } }
        public DataRow DrVentas { set { Ventas = value; } }

        private void FrmReporteVentas_Load(object sender, EventArgs e)
        {
            dsVentas.Tables["Ventas"].Rows.Clear();

            DataTable dtventas = dsVentas.Tables["Ventas"];


            this.reportViewer1.RefreshReport();
            this.reportViewer2.RefreshReport();
        }
    }
}
