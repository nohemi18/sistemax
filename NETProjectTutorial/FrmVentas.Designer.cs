﻿namespace NETProjectTutorial
{
    partial class FrmVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnver = new System.Windows.Forms.Button();
            this.dsVentas = new System.Data.DataSet();
            this.dtVentas = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dgvVentas = new System.Windows.Forms.DataGridView();
            this.bsVentas = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dsVentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVentas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnver
            // 
            this.btnver.Location = new System.Drawing.Point(242, 393);
            this.btnver.Name = "btnver";
            this.btnver.Size = new System.Drawing.Size(75, 23);
            this.btnver.TabIndex = 0;
            this.btnver.Text = "Ver";
            this.btnver.UseVisualStyleBackColor = true;
            this.btnver.Click += new System.EventHandler(this.btnver_Click);
            // 
            // dsVentas
            // 
            this.dsVentas.DataSetName = "NewDataSet";
            this.dsVentas.Tables.AddRange(new System.Data.DataTable[] {
            this.dtVentas});
            // 
            // dtVentas
            // 
            this.dtVentas.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dtVentas.TableName = "Ventas";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Codigo";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Producto";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Descripcion";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Cantidad";
            this.dataColumn4.DataType = typeof(int);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Precio";
            this.dataColumn5.DataType = typeof(double);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Total";
            this.dataColumn6.DataType = typeof(double);
            // 
            // dgvVentas
            // 
            this.dgvVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVentas.Location = new System.Drawing.Point(12, 12);
            this.dgvVentas.Name = "dgvVentas";
            this.dgvVentas.Size = new System.Drawing.Size(534, 375);
            this.dgvVentas.TabIndex = 2;
            // 
            // bsVentas
            // 
            this.bsVentas.DataMember = "Ventas";
            this.bsVentas.DataSource = this.dsVentas;
            // 
            // FrmVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 428);
            this.Controls.Add(this.dgvVentas);
            this.Controls.Add(this.btnver);
            this.Name = "FrmVentas";
            this.Text = "Productos Vendidos";
            this.Load += new System.EventHandler(this.FrmVentas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsVentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtVentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVentas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnver;
        private System.Data.DataSet dsVentas;
        private System.Data.DataTable dtVentas;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.DataGridView dgvVentas;
        private System.Windows.Forms.BindingSource bsVentas;
    }
}