﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmVentas : Form
    {
        private DataSet DsVentas;
        private BindingSource BsVentas;

        public DataSet Dsventas { get { return DsVentas; } set { DsVentas = value; } }
        public FrmVentas()
        {
            InitializeComponent();
            BsVentas = new BindingSource();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {
            BsVentas.DataSource = Dsventas;
            BsVentas.DataMember = Dsventas.Tables["Ventas"].TableName;
            dgvVentas.DataSource = BsVentas;
            dgvVentas.AutoGenerateColumns = true;

        }

        private void btnver_Click(object sender, EventArgs e)
        {

        }
    }
}
